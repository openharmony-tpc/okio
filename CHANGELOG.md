## v2.0.2-rc.0

Fix Sink member function exception caused by not using member variable filePath
Compile and build alarm modification.
Code check compilation build fatal issue modification

## v2.0.1

Verified on DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66).

## v2.0.0

The package management tool has been switched from npm to ohpm. 
Adapted to DevEco Studio: 3.1 Beta2 (3.1.0.400)  
Adapted to SDK: API9 Release (3.2.11.9).

## 1.0.1

Adapt to DevEco Studio 3.1 Beta1 and above versions.

## 1.0.0

1. Port and adapt okio to use with the JS language in the OpenHarmony framework.

