# okio

## Introduction

okio is a library that optimizes the input and output streams of the system through data flows, serialization, and file systems. It relies on system capabilities to provide string encoding and decoding, basic data type reading and writing, and file reading and writing. okio provides buffers for data containers and covers the features supported by NIO packages. Using okio as a component of the HTTP client makes it easier to access, store, and process data.

## How to Install

```javascript
ohpm install @ohos/okio 
```

## How to Use

```javascript
import okio from '@ohos/okio';
```

1. Writing and reading a UTF-8 string in the buffer:

  ```javascript
   performWriteUtf8() {
       var buffer = new okio.Buffer();
       buffer.writeUtf8("test");
       var readUtfValue = buffer.readUtf8();
   }
  ```

2. Writing and reading an integer (Int) in the buffer:

  ```javascript
   performWriteInt() {
       var buffer = new okio.Buffer();
       buffer.writeInt(10);
       var readInt = buffer.readInt();
   }
  ```

3. Writing and reading a string in the buffer:

  ```javascript
   performWriteString() {
       var buffer = new okio.Buffer();
       buffer.writeString("Test");
       var readString = buffer.readString();
   }
  ```

4. Writing and reading a little-endian integer in the buffer:

  ```javascript
   performWriteIntLe() {
       var buffer = new okio.Buffer();
       buffer.writeIntLe(25);
       var readIntLe = buffer.readIntLe();
   }
  ```

5. Writing and reading a short integer in the buffer:

  ```javascript
   performWriteShort() {
       var buffer = new okio.Buffer();
       buffer.writeShort(25);
       var readShort = buffer.readShort();
   }
  ```

6. Writing and reading a little-endian short integer in the buffer:

  ```javascript
   performWriteShortLe() {
       var buffer = new okio.Buffer();
       buffer.writeShortLe(100);
       var readShortLe = buffer.readShortLe();
   }
  ```

7. Writing and reading a byte in the buffer:

  ```javascript
   performWriteByte() {
       var buffer = new okio.Buffer();
       buffer.writeByte(9)
       var readByte = buffer.readByte();
   }
  ```

8. Writing and reading a UTF-8 code point in the buffer:

  ```javascript
   performWriteUtf8CodePoint() {
       var buffer = new okio.Buffer();
       buffer.writeUtf8CodePoint(99);
       var readUtf8CodePointValue = buffer.readUtf8CodePoint();
   }
  ```

9. Writing a Base64 encoded string into the byte string:

  ```javascript
   decodeBase64() {
       let decodeBase64 = byteStringCompanObj.decodeBase64('SGVsbG8gd29ybGQ=');
       let decodeBase64Value = JSON.stringify(decodeBase64);
   }
  ```

10. Writing a hexadecimal string into the byte string:

    ```javascript
    decodeHex() {
        let decodehex = byteStringCompanObj.decodeHex('48656C6C6F20776F726C640D0A');
        let decodeHexValue = JSON.stringify(decodehex);
    }
    ```

11. Writing a UTF-8 string into the byte string:

    ```javascript
    encodeUtf8() {
        let encodeUtf8 = byteStringCompanObj.encodeUtf8('Hello world #4 ❤ ( ͡ㆆ ͜ʖ ͡ㆆ)');
        let encodeUtf8Value = JSON.stringify(encodeUtf8);
    }
    ```

12. Writing a string into the byte string:

    ```javascript
    ofValue() {
        let ofData = byteStringCompanObj.of(["Hello", "world"]);
        let ofOutputValue = JSON.stringify(ofData);
    }
    ```

13. Writing content to a file:

     ```javascript
    writeFile() {
        let fileUri = '/data/data/com.openharmony.ohos.okioapplication/cache/test.txt';
        let writeInputValue = "openharmony";
        var sink = new okio.Sink(fileUri);
        var isAppend = false;
    	sink.write(writeInputValue,isAppend);
    }
     ```

14. Reading content from a file:

    ```javascript
    readFileValue() {
        let fileUri = '/data/data/com.openharmony.ohos.okioapplication/cache/test.txt';
        var source = new okio.Source(fileUri);
        source.read().then(function (data) {
            that.readValue = data;
        }).catch(function (error) {
            //Error
        });
    }
    ```

## Available APIs

### Buffer

| API                | Parameter                        | Return Value       | Description                                               |
| ---------------------- | ---------------------------- | ------------- | --------------------------------------------------- |
| writeUtf8              | data: string                 | Buffer        | Writes a UTF-8 string into this buffer.                       |
| readUtf8               | N/A                          | string        | Reads a UTF-8 string from this buffer.                               |
| writeInt               | data: int                    | Buffer        | Writes an integer into this buffer.                          |
| readInt                | N/A                          | int           | Reads an integer from this buffer.                          |
| writeString            | data: string                 | Buffer        | Writes a string into this buffer.                     |
| readString             | N/A                          | string        | Reads a string from this buffer.                               |
| writeIntLe             | data: intLe                  | Buffer        | Writes a little-endian integer into this buffer.                        |
| readIntLe              | N/A                          | intLe         | Reads a little-endian integer from this buffer.                        |
| writeShortLe           | data: shortLe                | Buffer        | Writes a little-endian short integer into this buffer.                      |
| readShortLe            | N/A                          | shortLe       | Reads a little-endian short integer from this buffer.                      |
| writeShort             | data: Short                  | Buffer        | Writes a short integer into this buffer.                        |
| readShort              | N/A                          | short         | Reads a short integer from this buffer.                        |
| writeByte              | data: byte                   | Buffer        | Writes a byte into this buffer.                         |
| readByte               | N/A                          | byte          | Reads a byte from this buffer.                         |
| writeUtf8CodePoint     | data: Utf8CodePoint          | Buffer        | Writes a UTF-8 code point into this buffer.                        |
| readUtf8CodePoint      | N/A                          | Utf8CodePoint | Reads a UTF-8 code point from this buffer.                        |
| readUtf8ByteCount      | byteCount                    | string        | Reads a string of length specified by **byteCount** from this buffer.  |
| writableSegment        | minimumCapacity              | int           | Obtain a writable segment based on the specified minimum capacity.          |
| writeSubString         | string, beginIndex, endIndex | Buffer        | Writes a substring of the string into this buffer. |
| writeUtf8BeginEndIndex | string, beginIndex, endIndex | Buffer        | Writes a substring of the UTF-8 string into this buffer. |
| getCommonResult        | pos: int                     | string        | Reads data of length **pos** from this buffer.      |
| skipByteCount          | bytecount: int               | void          | Skips the **bytecount**-specified number of bytes while reading from this buffer.                    |
| readByteArray          | byteCount                    | string        | Reads the **bytecount**-specified number of bytes from this buffer.|
| readFully              | sink: int                    | Buffer        | Reads data fully according to the given length from this buffer.                 |
| read                   | sink, offset,                | int           | Reads data starting from the position specified by **offset** for the **bytecount**-specified number of bytes.                    |

### ByteString

| API          | Parameter              | Return Value    | Description                                                        |
| ---------------- | ------------------ | ---------- | ------------------------------------------------------------ |
| ByteString       | data: buffer       | void       | Creates a **ByteString** instance from a data buffer.                        |
| decodeBase64     | data: Base64String | ByteString | Decodes the specified Base64 encoded string and returns the result as a **ByteString** instance.|
| decodeHex        | data: HexString    | ByteString | Decodes the specified hexadecimal string and returns the result as a **ByteString** instance.|
| encodeUtf8       | data: string       | ByteString | Encodes the specified string using UTF-8 and returns the result as a **ByteString** instance. |
| of               | data: array        | ByteString | Returns a new **ByteString** instance that copies the data from the specified array.                  |
| toAsciiLowercase | data: string       | ByteString | Converts the specified string into lowercase and returns the result as a **ByteString** instance.|
| toAsciiUppercase | data: string       | ByteString | Converts the specified string into uppercase and returns the result as a **ByteString** instance.|
| toByteArray      | N/A                | ByteArray  | Copies the byte array from this byte string and returns it as a **ByteArray** instance.             |
| getWithIndex     | index: int         | ByteValue  | Obtains the byte value at the specified index.                              |
| getSize          | N/A                | DataLength | Obtains the number of bytes in this byte string.                                    |
| internalArray    | N/A                | ByteArray  | Returns the internal byte array of this byte string without copying.                |
| hashCode         | N/A                | Hashcode   | Returns the hashcode of this byte string.                                    |
| compareToOther   | other: Buffer      | ByteArray  | Compares this byte string to another.<br>(a) Returns **0** if the two byte strings are equal in size and content.<br>(b) Returns **-1** if the first byte of this byte string is less than that of the other byte string.<br>(c) Returns **1** if the first byte of this byte string is greater than that of the other byte string. |

### FIleHandler(Sink, Source)

| API| Parameter                                      | Return Value| Description                                                        |
| ------ | ------------------------------------------ | ------ | ------------------------------------------------------------ |
| read   | N/A                                        | string | Reads content from a file and returns it as a string.                                      |
| write  | writeInputValue: string, isAppend: boolean | void   | Appends the specified string to the existing file if **isAppend** is **true**; writes the string to a new file if **isAppend** is **false**.|

## About obfuscation
- Code obfuscation, please see[Code Obfuscation](https://docs.openharmony.cn/pages/v5.0/zh-cn/application-dev/arkts-utils/source-obfuscation.md)
- If you want the okio library not to be obfuscated during code obfuscation, you need to add corresponding exclusion rules in the obfuscation rule configuration file obfuscation-rules.txt：
```
-keep
./oh_modules/@ohos/okio
```

## Constraints

This project has been verified in the following versions:

- Deveco Studio: 4.0 (4.0.3.512), SDK: API 10 (4.0.10.9)

## Directory Structure

````javascript
|---- okio  
|     |---- entry            # Sample code
|     |---- library          # okio library
|           |---- index.ets  # External APIs
|     |---- README_EN.md     # Readme                  
````

## How to Contribute

If you find any problem during the use, submit an [Issue](https://gitcode.com/openharmony-tpc/okio/issues) or [PR](https://gitcode.com/openharmony-tpc/okio/pulls) to us.

## License

This project is licensed under [Apache License 2.0](https://gitcode.com/openharmony-tpc/okio/blob/master/LICENSE).
